package com.example.perro

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.perro.ui.theme.PerroTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private lateinit var retrofit : Retrofit
    private var texto: String = "perro"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retrofit = retrofit2.Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        texto = obtenerDatos(retrofit)
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Text(text = texto)
                }
            }
        }
    }
    private fun obtenerDatos(retrofit : Retrofit) : String{
        var texto = "";
        CoroutineScope(Dispatchers.IO).launch {
            val call =
                retrofit.create(PerroApi::class.java).gerPerro1().execute()
            val perro = call.body()

            if(call.isSuccessful){
                texto = perro?.getmessage().toString()

            }else{
                texto = "Ha habido un error"
            }
        }
        Thread.sleep(2000)
        return texto;
    }
}