package com.example.perro;
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

    interface PerroApi {
        @Headers("Accept: application/json")

        @GET("breeds/image/random")
        fun gerPerro1(): Call<Perrito>


    }
}
